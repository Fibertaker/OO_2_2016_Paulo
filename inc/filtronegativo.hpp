#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include "filtros.hpp"

using namespace std;

class Negativo : public Filtros {
	public:
	Negativo();
	~Negativo();
	void AplicarFiltro(string magicnumber, int largura, int altura, int maxcolor, unsigned char** r, unsigned char** g, unsigned char** b);
};

#endif
