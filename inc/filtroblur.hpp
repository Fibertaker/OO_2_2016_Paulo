#ifndef FILTROBLUR_HPP
#define FILTROBLUR_HPP

#include "filtros.hpp"

using namespace std;

class Blur : public Filtros {
	public:
	Blur();
	~Blur();
	void AplicarFiltro(string magicnumber, int largura, int altura, int maxcolor, unsigned char** r, unsigned char** g, unsigned char** b);
};

#endif
