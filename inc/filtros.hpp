#ifndef FILTROS_HPP
#define FILTROS_HPP

#include <iostream>
#include <string>
#include "ppm.hpp"

using namespace std;

class Filtros{
	public:
	Filtros();
	virtual void AplicarFiltro();
};

#endif
