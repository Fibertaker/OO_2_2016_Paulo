#ifndef FILTROPRETOEBRANCO_HPP
#define FILTROPRETOEBRANCO_HPP

#include "filtros.hpp"

using namespace std;

class PretoeBranco : public Filtros {
	public:
	PretoeBranco();
	~PretoeBranco();
	void AplicarFiltro(string magicnumber, int largura, int altura, int maxcolor, unsigned char** r, unsigned char** g, unsigned char** b);
};

#endif
