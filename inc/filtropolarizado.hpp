#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include "filtros.hpp"

using namespace std;

class Polarizado : public Filtros {
	public:
	Polarizado();
	~Polarizado();
	void AplicarFiltro(string magicnumber, int largura, int altura, int maxcolor, unsigned char** r, unsigned char** g, unsigned char** b);
};

#endif
