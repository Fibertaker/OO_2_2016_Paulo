#ifndef PPM_HPP
#define PPM_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>

using namespace std;

class PPM {
    protected:
        unsigned char** r;
        unsigned char** g;
        unsigned char** b;
        string magicnumber;       
        int altura;
        int largura;        
        int maxcolor;
    public:
        PPM();
        ~PPM();

        char getImagem();
        unsigned char** getR();
        unsigned char** getG();
        unsigned char** getB();
        string getMagicNumber();
        int getAltura();
        int getLargura();
        int getMaxColor();
        void setImagem(char *imagem);
        void setR(unsigned char** r);
        void setG(unsigned char** g);
        void setB(unsigned char** b);
        void setMagicNumber(string magicnumber);
        void setAltura(int altura);
        void setLargura(int largura);
        void setMaxColor(int maxcolor);
        void read();
};

#endif
