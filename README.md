# EP1 - OO C++ (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Como Compilar e Executar.

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Coloque a(s) imagem(s) desejada(s) dentro do diretório /doc na pasta do Programa;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**

## Como abrir, ler e aplicar filtro na imagem .PPM.

* Compile e execute o programa;
* 1 - Digite o nome da imagem sem a extenção: 
		Ex: Digite imagem se quiser abrir a "imagem.ppm"
* 2 - Escolha o filtro desejado dentre os 4 disponíveis;
* 2.1 - Caso escolha o "Filtro de Média", escolha a dimensão da máscara de filtro NxN desejada (aonde N>0 && N<=20 por motivos de excesso de desfoco);
* 3 - Se a Imagem for encontrada e for PPM P6 ela sera lida, caso contrário ira aparecer uma mensagem de erro no terminal;
* 4 - Digite o nome final da imagem filtrada sem a extenção:
		Ex: Digite copy se quiser salvar a imagem como "copy.ppm"
* 5 - Se todos os passos anteriores forem completados a operação sera feita com êxito;
