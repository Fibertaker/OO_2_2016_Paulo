#include "ppm.hpp"
#include "filtronegativo.hpp"

using namespace std;

Negativo::Negativo(){}

void Negativo::AplicarFiltro(string magicnumber, int largura, int altura, int maxcolor, unsigned char** r, unsigned char** g, unsigned char** b){

 	string foto,nome;
    	char blank='\n';

	cout << "\n" << "Digite o nome de saída da imagem: " << "\n" << endl;
	cin >> foto;
	nome = "doc/" + foto + ".ppm";
	ofstream f1(nome.c_str());

    	f1 << magicnumber;
    	f1.put(blank);
    	f1 << largura << " " << altura;
    	f1.put(blank);
    	f1 << maxcolor;
    	f1.put(blank);

	for(int i=0;i<altura;i++){
		for(int j=0;j<largura;j++){

			r[i][j] = maxcolor - (int)r[i][j];
			f1.write((char*)&r[i][j],1);

			g[i][j] = maxcolor - (int)g[i][j];
			f1.write((char*)&g[i][j],1);

			b[i][j] = maxcolor - (int)b[i][j];
			f1.write((char*)&b[i][j],1);
		}
	}
	f1.close();
 	cout << "\n" << "Operação feita com êxito!"<< "\n" << endl;
 }
