#include "ppm.hpp"
#include "filtropretoebranco.hpp"

using namespace std;

PretoeBranco::PretoeBranco(){}

void PretoeBranco::AplicarFiltro(string magicnumber, int largura, int altura, int maxcolor, unsigned char** r, unsigned char** g, unsigned char** b){

	int grayscale_value;
 	string foto,nome;
    	char blank='\n';

	cout << "\n" << "Digite o nome de saída da imagem: " << "\n" << endl;
	cin >> foto;
	nome = "doc/" + foto + ".ppm";
	ofstream f3(nome.c_str());

   	f3 << magicnumber;
    	f3.put(blank);
    	f3 << largura << " " << altura;
    	f3.put(blank);
    	f3 << maxcolor;
    	f3.put(blank);

	for(int i=0; i<altura; i++){
    	for(int j=0; j<largura; j++){
            grayscale_value = (0.299 * r[i][j]) + (0.587 * g[i][j]) + (0.144 * b[i][j]);
		
		if(grayscale_value>255){grayscale_value = 255;}
		else if(grayscale_value<0){grayscale_value = 0;}

            r[i][j] = grayscale_value;
		    f3.write((char*)&r[i][j],1);

            g[i][j] = grayscale_value;
			f3.write((char*)&g[i][j],1);

            b[i][j] = grayscale_value;
			f3.write((char*)&b[i][j],1);
    	}
	}
	f3.close();
 	cout << "\n" << "Operação feita com êxito!"<< "\n" << endl;
 }
