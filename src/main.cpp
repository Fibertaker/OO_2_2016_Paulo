#include "ppm.hpp"
#include "filtros.hpp"
#include "filtronegativo.hpp"
#include "filtropolarizado.hpp"
#include "filtropretoebranco.hpp"
#include "filtroblur.hpp"

int main(){

	int choice;
	PPM *image = new PPM();
	image->read();
	
	unsigned char** r = image->getR();
        unsigned char** g = image->getG();
        unsigned char** b = image->getB();   
	string magicnumber = image->getMagicNumber();  
        int altura = image->getAltura();
        int largura = image->getLargura();       
        int maxcolor = image->getMaxColor();

	if (magicnumber != "P6") {
		cout << "Tipo de imagem não suportado." << endl;
		exit(0); 
	}
	else{
	cout << "\n" << "Escolha um filtro: " << endl;
	cout << "1.Filtro Negativo." << "\n" << "2.Filtro Polarizado." << "\n" << "3.Filtro Preto e Branco." << "\n" << "4.Filtro de Média." << "\n" << endl;
	cin >> choice;
	
	switch(choice)	{
		case (1):{Negativo *negativo = new Negativo();
		negativo -> AplicarFiltro(magicnumber,largura,altura,maxcolor,r,g,b);
		break;}

		case (2):{Polarizado *polarizado = new Polarizado();
		polarizado -> AplicarFiltro(magicnumber,largura,altura,maxcolor,r,g,b);
		break;}

		case (3):{PretoeBranco *pretoebranco = new PretoeBranco();
		pretoebranco -> AplicarFiltro(magicnumber,largura,altura,maxcolor,r,g,b);
		break;}

		case (4):{Blur *blur = new Blur();
		blur -> AplicarFiltro(magicnumber,largura,altura,maxcolor,r,g,b);
		break;}

		default:{cout << "Opção não disponível. Execute Novamente..." << endl;
		exit(0);};
		}
	}
	delete(image);

return 0;
}
