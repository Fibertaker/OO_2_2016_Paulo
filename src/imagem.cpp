#include "ppm.hpp"

using namespace std;

	PPM::PPM() {}
	PPM::~PPM(){}
	
	unsigned char** PPM::getR(){
		return r;
	}
	void PPM::setR(unsigned char** r){
		this -> r = r;
	}
	unsigned char** PPM::getG(){
		return g;
	}
	void PPM::setG(unsigned char** g){
		this -> g = g;
	}
	unsigned char** PPM::getB(){
		return b;
	}
	void PPM::setB(unsigned char** b){
		this -> b = b;
	}
	string PPM::getMagicNumber() {
		return magicnumber;
	}
	void PPM::setMagicNumber(string magicnumber) {
	    this -> magicnumber = magicnumber;
	}
	int PPM::getLargura() {
	    return largura;
	}
	void PPM::setLargura(int largura) {
	    this -> largura = largura;
	}
	int PPM::getAltura() {
	    return altura;
	}
	void PPM::setAltura(int altura) {
	    this -> altura = altura;
	}
	int PPM::getMaxColor() {
	    return maxcolor;
	}
	void PPM::setMaxColor(int maxcolor) {
	    this -> maxcolor = maxcolor;
	}

void eatcomentario(ifstream &file){
	char buffer[1024];
	char p1,p2,p3,p4,p5;
	while(p1=file.peek(),p1 == '\n' || p1 == '\r')
		file.get();
	if(p1 == '#'){
		file.getline(buffer,1023);
		p2=file.peek();
		if(p2 == '#'){
			file.getline(buffer,1023);}
			p3=file.peek();
			if(p3 == '#'){
				file.getline(buffer,1023);}
				p4=file.peek();
				if(p4 == '#'){
					file.getline(buffer,1023);}
					p5=file.peek();
					if(p5 == '#'){
						file.getline(buffer,1023);}
		}
}

void PPM::read(){
	string magicnumber,s,mn,comentario,photo,barra,entrada;
	int largura, altura, maxcolor,i,j;
	cout << "Digite o nome de entrada da imagem: ";
	cin >> photo;
	entrada = "doc/" + photo + ".ppm";
	ifstream file(entrada.c_str());
	
	if(!file.is_open()){
		cout << "Não foi possível abrir o arquivo." << endl;
		exit(0);
	}
	else{
		eatcomentario(file);
		file >> magicnumber;
 		setMagicNumber(magicnumber);
		eatcomentario(file);
		file >> largura;
		setLargura(largura);
		eatcomentario(file);
		file >> altura;
		setAltura(altura);
		eatcomentario(file);
		file >> maxcolor;
		setMaxColor(maxcolor);		
		eatcomentario(file);

		r = new unsigned char*[altura];
		g = new unsigned char*[altura];
		b = new unsigned char*[altura];

		for(i=0;i<altura;i++){
			r[i] = new unsigned char[largura];
			g[i] = new unsigned char[largura];
			b[i] = new unsigned char[largura];
			for(j=0;j<largura;j++){
				file.read((char*)&r[i][j],1);
				file.read((char*)&g[i][j],1);
				file.read((char*)&b[i][j],1);
			}
		}
	}
	file.close();
}
