#include "ppm.hpp"
#include "filtroblur.hpp"

using namespace std;

Blur::Blur(){}

void Blur::AplicarFiltro(string magicnumber, int largura, int altura, int maxcolor, unsigned char** r, unsigned char** g, unsigned char** b){

	int valor,valuer,valueg,valueb,value;
 	string foto,nome;
    	char blank='\n';

	cout << "\n" << "Entre com a dimensão da máscara de filtro NxN (Até 20x20 por motivos de excesso de desfoco): " << "\n" << endl;
	cin >> valor;
    while(valor<=0 || valor>20){
		cout << "\n" << "Valor não permitido: " << "\n" << endl;
		cin >> valor;
	}
	cout << "\n" << "Digite o nome de saída da imagem: " << "\n" << endl;
	cin >> foto;
	nome = "doc/" + foto + ".ppm";
	ofstream f4(nome.c_str());

    	f4 << magicnumber;
    	f4.put(blank);
    	f4 << largura << " " << altura;
    	f4.put(blank);
    	f4 << maxcolor;
    	f4.put(blank);
	
	int i=0;
   	while(i<altura){
        	for(int j=0; j<largura; j++) {
           	valuer = 0;
           	valueg = 0;
           	valueb = 0;
           		for(int x=-valor; x<=valor; x++) {
                		for(int y=-valor; y<=valor;y++){
					if((i+x)>=0 && (x+i) < altura){
						if((j+y)>=0 && (j+y) < largura){
                    					valuer += (int)r[i+x][j+y];
                    					valueg += (int)g[i+x][j+y];
                    					valueb += (int)b[i+x][j+y];
							value+=1;
						}
					}
                		}
           		}
            	//value /= div;
		valuer = valuer/value;
		valueg = valueg/value;
		valueb = valueb/value;

            	r[i][j] = (unsigned char)valuer;
		    	f4.write((char*)&r[i][j], sizeof(unsigned char));
            	g[i][j] = (unsigned char)valueg;
			    f4.write((char*)&g[i][j], sizeof(unsigned char));
                b[i][j] = (unsigned char)valueb;
			    f4.write((char*)&b[i][j], sizeof(unsigned char));
		
           	valuer = 0;
           	valueg = 0;
           	valueb = 0;
		    value = 0;

        	}
	    i++;
    	}
	f4.close();
 	cout << "\n" << "Operação feita com êxito!"<< "\n" << endl;
 }
