#include "ppm.hpp"
#include "filtropolarizado.hpp"

using namespace std;

Polarizado::Polarizado(){}

void Polarizado::AplicarFiltro(string magicnumber, int largura, int altura, int maxcolor, unsigned char** r, unsigned char** g, unsigned char** b){

 	string foto,nome;
    	char blank='\n';

	cout << "\n" << "Digite o nome de saída da imagem: " << "\n" << endl;
	cin >> foto;
	nome = "doc/" + foto + ".ppm";
	ofstream f2(nome.c_str());

    	f2 << magicnumber;
    	f2.put(blank);
    	f2 << largura << " " << altura;
    	f2.put(blank);
    	f2 << maxcolor;
    	f2.put(blank);

	for(int i=0; i<altura; i++){
	    for(int j=0; j<largura; j++){

	           if((int)r[i][j] > (maxcolor/2)){
			r[i][j] = (unsigned char)maxcolor;
	           }else{
			r[i][j] = 0;
	           }
	            f2.write((char*)&r[i][j],sizeof(unsigned char));
	
	
	
	           if((int)g[i][j] > (maxcolor/2)){
	               	g[i][j] = (unsigned char)maxcolor;
	           }else{
			g[i][j] = 0;
	           }
	            f2.write((char*)&g[i][j],sizeof(unsigned char));
	
	
	
	           if((int)b[i][j] > (maxcolor/2)){	               
			b[i][j] = (unsigned char)maxcolor;
	           }else{
			b[i][j] = 0;
	           }
			f2.write((char*)&b[i][j],sizeof(unsigned char));
	    	}
	}
	f2.close();
 	cout << "\n" << "Operação feita com êxito!"<< "\n" << endl;
 }
